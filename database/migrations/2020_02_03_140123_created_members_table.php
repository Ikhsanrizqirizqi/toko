<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatedMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignetBigInteger('member_category_id');
            $table->string('full_name');
            $table->date('dob');
            $table->string('address');
            $table->string('gender');
            $table->string('barcode');
            $table->timestamps();

            $table->foreign('member_category_id')->references('id')->on('member_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
