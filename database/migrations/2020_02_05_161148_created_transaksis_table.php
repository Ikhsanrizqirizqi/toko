<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatedTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('trx_number');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('member_id');
            $table->integer('quantity');
            $table->string('discount');
            $table->integer('total');
            $table->timestamps();


            $table->foreign('product_id')->refrences('id')->on('product');
            $table->foreign('member_id')->refrences('id')->on('member');
           


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksis');
    }
}
