@extends('layouts.template')

@section('content')


     
  <!-- Begin Page Content -->
  <div class="container-fluid">

<!-- Page Heading -->
<div class="card shadow mb-4">
  <div class="card-header py-2">
     <h1 class="h3 mb-1 text-gray">Transaksi</h1>

<!-- DataTales Example -->
<div class="card shadow mb-3">
  <div class="card-header py-2">
      <li>
        <a href="{{ route('transaksi.create') }}">
          <button type="button" class="btn btn-primary btn-sm">Tambah</button>
        </a>
      </li>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr style="text-align: center;">
            <th>No</th>
            <th>Nomor Trx</th>
            <th>Nama Produk </th>
            <th>Nama Lengkap</th>
            <th>Kuantitas</th>
            <th>Discount</th>
            <th>Total</th>
            <th>Pilihan</th>
          </tr>
        </thead>
        
        <tbody>
              @foreach ($transaksis as $i => $item)
              <tr style="text-align: center;">
                  <td>{{ $i+1 }}</td>
                  <td>{{ $item->trx_number }}</td>
                  <td>{{ $item->categoryRef->name }}</td>
                  <td>{{ $item->categRef->full_name }}</td>
                  <td>{{ $item->quantity }}</td>
                  <td>Rp.{{ $item->discount }}</td>
                  <td>Rp.{{ $item->total }}</td>
                  <td>
                  <form action="{{ route('transaksi.destroy',$item->id) }}" 
                    method="post">
                    <a href="{{ route('transaksi.edit',$item->id) }}">
                      <button type="button" class="btn btn-success">Ubah</button>
                    </a>
                    @csrf 
                    @method('delete')
                      <button type="submit" class="btn btn-warning" 
                      onclick="return confirm('Yakin Nih Dihapus ?')">Hapus</button>
                
              </a>
                    </form>
                </td>
              </tr>
              @endforeach
              </tbody>  
        
      </table>
    </div>
  </div>
</div>
@endsection