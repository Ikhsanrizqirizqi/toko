@extends('layouts.template')

@section('content')

<div class="container">
<div class="x_panel">
    <div class="x_title">
      <div class="card shadow mb-4">
  <div class="card-header py-2">
     <h1 class="h3 mb-1 text-gray">Tambah Data Transaksi</h1>

<!-- DataTales Example -->
<div class="card shadow mb-3">
  <div class="card-header py-2">

    <div class="card body">                           
    <form action="{{ route ('transaksi.store') }}" method="post">
    @csrf 
    <form role="form">
        <div class="col-md-6">
        <div class="form-group">
                <label for="products">Nama Produk</label>
                <select class="form-control" id="products" name="products">
                            <option>chose...</option>
                                @foreach ($product as $item)
                                <option value="{{ $item->id }}"> {{ $item->name }}</option>
                                @endforeach
                        </select>
            </div>

        <div class="form-group">
                <label for="member">Nama</label>
                <select class="form-control" id="member" name="member">
                            <option>chose...</option>
                                @foreach ($member as $item)
                                <option value="{{ $item->id }}"> {{ $item->full_name }}</option>
                                @endforeach
                        </select>
            </div>

        <div class="form-group">
                <label for="quantity">Jumlah</label>
                <input type="text" name="quantity" class="form-control" id="quantity" rows="1" placeholder="Masukkan Deskripsi Barang">
            </div>
            <button type="submit" class="btn btn-danger">Simpan</button>
            <a href="/transaksi" class="btn btn-warning">Kembali</a>
    </form>
    </div> 
    </div> 
    </div>
    </div>                                 
@endsection