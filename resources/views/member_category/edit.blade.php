@extends('layouts.template')

@section('content')

<div class="container">
<h3>Ubah Data Kategori Anggota</h3>
    <div class="card body">                           
    
    <form action="{{ route('member_category.update', $category->id) }}" method="post">
    @csrf
    @method('put') 
    <form role="form">
         <div class="col-md-6">
            <div class="form-group">
                <label for="name">Nama</label>
                <input type="text" name="name" class="form-control" id="name" placeholder="Masukkan Nama Barang" value="{{ $category->name }}">
            </div>
        <div class="form-group">
                <label for="discount">Diskon</label>
                <input type="text" name="discount" class="form-control" id="name" placeholder="Masukkan Nama Barang" value="{{ $category->discount }}">
            </div>
        
            <button type="submit" class="btn btn-danger">Simpan</button>
            <a href="/member_category" class="btn btn-warning">Kembali</a>
    </form>
    </div> 
    </div>                                  
@endsection