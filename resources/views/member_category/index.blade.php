@extends('layouts.template')

@section('content')


     
  <!-- Begin Page Content -->
  <div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-1 text-gray">Kategori Anggota</h1>

<!-- DataTales Example -->
<div class="card shadow mb-3">
  <div class="card-header py-2">
      <li>
        <a href="{{ route('member_category.create') }}">
          <button type="button" class="btn btn-primary btn-sm">Tambah</button>
        </a>
      </li>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr style="text-align: center;">
            <th>No</th>
            <th>Nama</th>
            <th>Diskon</th>
            <th>Pilihan</th>
          </tr>
        </thead>
        
        <tbody>
              @foreach ($category as $i => $item)
              <tr style="text-align: center;">
                  <td>{{ $i+1 }}</td>
                  <td>{{ $item->name }}</td>
                  <td>{{ $item->discount }}%</td>
                  <td>
                    <form action="{{ route('member_category.destroy',$item->id) }}" 
                    method="post">
                    <a href="{{ route('member_category.edit',$item->id) }}">
                      <button type="button" class="btn btn-success">Ubah</button>
                    </a>
                    @csrf 
                    @method('delete')
                      <button type="submit" class="btn btn-warning" 
                      onclick="return confirm('Yakin Nih Dihapus ?')">Hapus</button>
                
              </a>
                    </form>
                </td>
              </tr>
              @endforeach
              </tbody>  
        
      </table>
    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

</div>
@endsection