@extends('layouts.template')

@section('content')


     
  <!-- Begin Page Content -->
  <div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-1 text-gray">Barang</h1>

<!-- DataTales Example -->
<div class="card shadow mb-3">
  <div class="card-header py-2">
      <li>
        <a href="{{ route('product.create') }}">
          <button type="button" class="btn btn-primary btn-sm">Tambah</button>
        </a>
      </li>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr style="text-align: center;">
            <th>No</th>
            <th>Nama</th>
            <th>Gambar</th>
            <th>Produk_Kategori</th>
            <th>Deskripsi</th>
            <th>Harga</th>
            <th>Pilihan</th>
          </tr>
        </thead>
        
        <tbody>
              @foreach ($category as $i => $item)
              <tr style="text-align: center;">
                  <td>{{ $i+1 }}</td>
                  <td>{{ $item->name }}</td>
                  <td> <img src="{{ URL::to('/') }}/uploads/products/{{$item->image}}"
                  class="img-thumbnail" width="100px" height="100px" /></td>
                  <td>{{ $item->categoryRef->name }}</td>
                  <td>{{ $item->desc }}</td>
                  <td>{{ $item->price }}</td>
                  <td>
                    <form action="{{ route('product.destroy',$item->id) }}" method="post">
                    <a href="{{ route('product.edit',$item->id) }}">
                      <button type="button" class="btn btn-success">Ubah</button>
                    </a>
                    @csrf 
                    @method('delete')
                      <button type="submit" class="btn btn-warning" 
                      onclick="return confirm('Yakin Nih Dihapus ?')"><i class="fas fa-trash"></i></button>
              <a href="{{ route('product.show',$item->id) }}" class="btn btn-info btn-circle">
                <i class="fas fa-list"></i>
               </a>

                    </form>
                </td>
              </tr>
              @endforeach
              </tbody>  
        
      </table>
    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer bg-white">
<div class="container my-auto">
<div class="copyright text-center my-auto">
  <span>Copyright &copy; Your Website 2019</span>
</div>
</div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
<i class="fas fa-angle-up"></i>
</a>
@endsection