@extends('layouts.template')

@section('content')

<div class="container">
<div class="x_panel">
    <div class="x_title">
     <h1 class="h3 mb-1 text-gray">Tambah Data Barang</h1>

<!-- DataTales Example -->


    <div class="card body">                           
    <form action="{{ route ('product.store') }}" method="post" enctype="multipart/form-data">
    @csrf 
    <form role="form">
         <div class="col-md-6">
            <div class="form-group">
                <label for="name">Nama</label>
                <input type="text" name="name" class="form-control" id="name" placeholder="Masukkan Nama Barang">
            </div>
            <br />
        <div class="form-group">
                <label for="image">Gambar</label>
                <input type="file" name="image" class="form-control" id="image" placeholder="Masukkan Gambar">
            </div>
        <div class="form-group">
                <label for="category">Produk Kategori</label>
                            <select class="form-control" id="category" name="category">
                            <option>chose...</option>
                                @foreach ($category as $item)
                                <option value="{{ $item->id }}"> {{ $item->name }}</option>
                                @endforeach
                        </select>
            </div>
        <div class="form-group">
                <label for="desc">Deskripsi</label>
                <input type="text" name="desc" class="form-control" id="desc" placeholder="Masukkan Deskripsi Barang">
            </div>
        <div class="form-group">
                <label for="price">Harga</label>
                <input type="text" name="price" class="form-control" id="price" placeholder="Masukkan Harga Barang">
            </div>
            <button type="submit" class="btn btn-danger">Simpan</button>
            <a href="/product" class="btn btn-warning">Kembali</a>
    </form>
    </div> 
    </div> 
    </div>
    </div>
                                     
@endsection