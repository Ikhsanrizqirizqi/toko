@extends('layouts.template')

@section('content')

<div class="container">
<div class="x_panel">
    <div class="x_title">
     <h1 class="h3 mb-1 text-gray">Ubah Data Barang</h1>

<!-- DataTales Example -->


    <div class="card body">                           
    <form action="{{ route ('product.update', $product->id) }}" method="post" enctype="multipart/form-data">
    @csrf
    @method('put') 
    <form role="form">
         <div class="col-md-6">
            <div class="form-group">
                <label for="name">Nama</label>
                <input type="text" name="name" class="form-control" id="name" placeholder="Masukkan Nama Barang" value="{{ $product->name }}">
            </div>
            <br />
        <div class="form-group">
                <label for="image">Gambar</label>
                <input type="file" name="image" class="form-control" id="image" placeholder="Masukkan Gambar">
            </div>
        <div class="form-group">
                <label for="category">Produk Kategori</label>
                            <select class="form-control" id="category" name="category">
                            <option>chose...</option>
                                @foreach ($category as $item)
                                <option value="{{ $item->id }}" {{ $product->product_category_id == $item->id ? 'selected' : ''}}> {{ $item->name }}</option>
                                @endforeach
                        </select>
            </div>
        <div class="form-group">
                <label for="desc">Deskripsi</label>
                <input type="text" name="desc" class="form-control" id="desc" placeholder="Masukkan Deskripsi Barang" value="{{ $product->desc }}">
            </div>
        <div class="form-group">
                <label for="price">Harga</label>
                <input type="text" name="price" class="form-control" id="price" placeholder="Masukkan Harga Barang" value="{{ $product->price }}" rows="1">
            </div>
            <button type="submit" class="btn btn-danger">Simpan</button>
            <a href="/product" class="btn btn-warning">Kembali</a>
    </form>
    </div> 
    </div> 
    </div>
    </div>
                                     
@endsection