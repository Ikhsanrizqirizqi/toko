@extends('layouts.template')

@section('content')

<div class="col-xl-6 col-lg-0">
              <div class="card shadow mb-3">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Tambah Kategori Barang</h6>
                  <form action="{{ route('product_category.store') }}" method="post">
                  @csrf
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" 
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    </a>
                  </div>
                </div>
              
            
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-area">
                  <div class="chartjs-size-monitor">
                  <div class="chartjs-size-monitor-expand">
                    <div class="form-group">
                        <label for="name">Nama Barang</label>
                        <input type="text" class="form-control" name="name" placeholder="Ketik Nama Barang">
                    </div>
                    <div class="form-group">
                        <label for="desc">Deskripsi</label>
                        <input type="text" class="form-control" name="desc" placeholder="Ketik Deskripsi">
                    </div>
                    <button type="submit" class="btn btn-danger">Simpan</button>
                 </div>
              </div>
              </div>
              </div>
              </div>
                </div>


                 
    
                 
                
@endsection





