@extends('layouts.template')

@section('content')

<div class="container">
<div class="x_panel">
    <div class="x_title">
      <div class="card shadow mb-4">
  <div class="card-header py-2">
     <h1 class="h3 mb-1 text-gray">Ubah Anggota</h1>

<!-- DataTales Example -->
<div class="card shadow mb-3">
  <div class="card-header py-2">

    <div class="card body">                           
    <form action="{{ route ('member.update', $member->id) }}" method="post">
    @csrf 
    @method('put')
    <form role="form">
        <div class="col-md-6">
            <div class="form-group">
                <label for="category">Nama</label>
                <select class="form-control" id="category" name="category">
                            <option>chose...</option>
                                @foreach ($category as $item)
                                <option value="{{ $item->id }}" > {{ $item->name }}</option>
                                @endforeach
                        </select>
            </div>
            <div class="form-group">
                <label for="full_name">Nama Lengkap</label>
                <input type="text" name="full_name" class="form-control" id="full_name" placeholder="Masukkan Nama Lengkap" value="{{$member->full_name}}">
            </div>
            <div class="form-group">
                <label for="dob">Tanggal Lahir</label>
                <input type="date" name="dob" class="form-control" id="dob" placeholder="Masukkan Tanggal Lahir" value="{{$member->dob}}">
            </div>
            <div class="form-group">
                <label for="address">Alamat</label>
                <input type="text" name="address" class="form-control" id="address" placeholder="Masukkan Alamat" value="{{$member->address}}">
            </div>
            <div class="form-group">
                <label class="mr-4">Jenis Kelamin</label>
                                    <div class="form-group">
                                        <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" name="gender" id="gender1" value="laki-laki" class="custom-control-input">
                                        <label class="custom-control-label" for="gender1">Laki-Laki</label>
                                        </div>
                                    </div>

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" name="gender" id="gender2" value="perempuan" class="custom-control-input" >
                                        <label class="custom-control-label" for="gender2">Perempuan</label>
                                        </div>
                                    </div>
            </div>
            <div class="form-group">
                <label for="barcode">Barcode</label>
                <input type="text" name="barcode" class="form-control" id="barcode" placeholder="Masukkan Nama Barang" value="{{$member->barcode}}">
            </div>

            <button type="submit" class="btn btn-danger">Simpan</button>
            <a href="/member" class="btn btn-warning">Kembali</a>
    </form>
    </div> 
    </div> 
    </div>
    </div>                                 
@endsection