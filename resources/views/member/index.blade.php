@extends('layouts.template')

@section('content')


     
  <!-- Begin Page Content -->
  <div class="container-fluid">

<!-- Page Heading -->
<div class="card shadow mb-4">
  <div class="card-header py-2">
     <h1 class="h3 mb-1 text-gray">Anggota</h1>

<!-- DataTales Example -->
<div class="card shadow mb-3">
  <div class="card-header py-2">
      <li>
        <a href="{{ route('member.create') }}">
          <button type="button" class="btn btn-primary btn-sm">Tambah</button>
        </a>
      </li>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr style="text-align: center;">
            <th>No</th>
            <th>Kategori Anggota</th>
            <th>Nama lengkap</th>
            <th>Tanggal Lahir</th>
            <th>Alamat</th>
            <th>Gender</th>
            <th>Barcode</th>
            <th>Pilihan</th>
          </tr>
        </thead>
        
        <tbody>
              @foreach ($category as $i => $item)
              <tr style="text-align: center;">
                  <td>{{ $i+1 }}</td>
                  <td>{{ $item->categoryRef->name }}</td>
                  <td>{{ $item->full_name }}</td>
                  <td>{{ $item->dob }}</td>
                  <td>{{ $item->address }}</td>
                  <td>{{ $item->gender }}</td>
                  <td>{{ $item->barcode }}</td>
                  <td>
                  <form action="{{ route('member.destroy',$item->id) }}" 
                    method="post">
                    <a href="{{ route('member.edit',$item->id) }}">
                      <button type="button" class="btn btn-success">Ubah</button>
                    </a>
                    @csrf 
                    @method('delete')
                      <button type="submit" class="btn btn-warning" 
                      onclick="return confirm('Yakin Nih Dihapus ?')"><i class="fas fa-trash"></i></button>
                      <a href="{{ route('member.show',$item->id) }}" class="btn btn-info btn-circle">
                <i class="fas fa-list"></i>
               </a>
                    </form>
                </td>
              </tr>
              @endforeach
              </tbody>  
        
      </table>
    </div>
  </div>
</div>
@endsection