@extends('layouts.template')

@section('content')


     
  <!-- Begin Page Content -->
  <div class="container-fluid">

<!-- Page Heading -->
<div class="card shadow mb-4">
  <div class="card-header py-2">
     <h1 class="h3 mb-1 text-gray">Detail Anggota</h1>

<!-- DataTales Example -->
<div class="card shadow mb-3">
  <div class="card-header py-2">
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr style="text-align: center;">
            <th>No</th>
            <th>No Transaksi</th>
            <th>Nama lengkap</th>
            <th>Nama Product</th>
            <th>Kuantitas</th>
            <th>Diskon</th>
            <th>Total</th>
          </tr>
        </thead>
        
        <tbody>
              @foreach ($member->pembeli as $i => $item)
              <tr style="text-align: center;">
                  <td>{{ $i+1 }}</td>
                  <td>{{ $item->trx_number }}</td>
                  <td>{{ $item->categRef->full_name }}</td>
                  <td>{{ $item->categoryRef->name }}</td>
                  <td>{{ $item->quantity }}</td>
                  <td>{{ $item->discount }}</td>
                  <td>{{ $item->total }}</td>
                  <td>
                </td>
              </tr>
              @endforeach
              </tbody>  
        
      </table>
    </div>
  </div>
</div>
@endsection