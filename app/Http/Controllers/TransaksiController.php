<?php

namespace App\Http\Controllers;

use App\Transaksi;
use App\Product;
use App\Member;
use Illuminate\Http\Request;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaksis = Transaksi::with('categoryRef','categRef')->get();
        return view('transaksi.index', compact('transaksis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $transaksis = Transaksi::all();
        $product = Product::all();
        $member = Member::all();
        return view('transaksi.create', compact('product','member'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $trx_number = str_replace("-", "", date("Y-m-d")).substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3);
       $price = Product::where('id', $request->products)->first()->price;
       $discountrate = Member::where('id', $request->member)->with('categoryRef')->first()->categoryRef->discount;
       $discount = $price * $request->quantity * $discountrate / 100;
       $total = $price * $request->quantity - $discount;

       $transaksi = new Transaksi;
       $transaksi->trx_number = $trx_number;
       $transaksi->product_id = $request->products;
       $transaksi->member_id = $request->member;
       $transaksi->quantity = $request->quantity;
       $transaksi->discount = $discount;
       $transaksi->total = $total;
       $transaksi->save();
       return redirect()->route('transaksi.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function show(Transaksi $transaksi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaksi $transaksi)
    {
        $transaksis = Transaksi::all();
        $product = Product::all();
        $member = Member::all();
        return view('transaksi.edit', compact('member', 'product', 'transaksi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaksi $transaksi)
    {
        $trx_number = str_replace("-", "", date("Y-m-d")).substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3);
       $price = Product::where('id', $request->products)->first()->price;
       $discountrate = Member::where('id', $request->member)->with('categoryRef')->first()->categoryRef->discount;
       $discount = $price * $request->quantity * $discountrate / 100;
       $total = $price * $request->quantity - $discount;

       
       $transaksi->trx_number = $trx_number;
       $transaksi->product_id = $request->products;
       $transaksi->member_id = $request->member;
       $transaksi->quantity = $request->quantity;
       $transaksi->discount = $discount;
       $transaksi->total = $total;
       $transaksi->save();
       return redirect()->route('transaksi.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaksi $transaksi)
    {
        $transaksi->delete();
        return redirect()->route('transaksi.index');
    }
}
