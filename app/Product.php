<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function categoryRef()
    {
      return $this->belongsTo(ProductCategory::class, 'product_category_id');
    }
    public function pembeli()
    {
      return $this->hasMany(Transaksi::class, 'product_id');
    }
}
