<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberCategory extends Model
{
    public function categoryRef()
    {
      return $this->belongsTo(MemberCategory::class, 'member_category_id');
    }
}